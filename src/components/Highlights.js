import { Row, Col, Card } from 'react-bootstrap';

export default function Highlights(){ 
    return (
        <Row className = 'mt-3 mb-3'>
            <Col xs={12} md={4}>
                <Card style={{ width: '18rem' }} className="cardHighlight p-3">
                    <Card.Body>
                        <Card.Title>
                            <h2>Learn From Home</h2>
                        </Card.Title>
                        <Card.Text>
                        Lorem Ipsum, dizgi ve baskı endüstrisinde kullanılan mıgır metinlerdir. Lorem Ipsum, adı bilinmeyen bir matbaacının bir hurufat numune kitabı oluşturmak üzere bir yazı galerisini alarak karıştırdığı 1500'lerden beri endüstri standardı sahte metinler olarak kullanılmıştır. 
                        </Card.Text>
                    </Card.Body>
                </Card>
            </Col>
            <Col xs={12} md={4}>
            <Card style={{ width: '18rem' }} className="cardHighlight p-3">
                <Card.Body>
                    <Card.Title>
                        <h2>Study Now Pay Later</h2>
                    </Card.Title>
                    <Card.Text>
                    Lorem Ipsum, dizgi ve baskı endüstrisinde kullanılan mıgır metinlerdir. Lorem Ipsum, adı bilinmeyen bir matbaacının bir hurufat numune kitabı oluşturmak üzere bir yazı galerisini alarak karıştırdığı 1500'lerden beri endüstri standardı sahte metinler olarak kullanılmıştır. 
                    </Card.Text>
                </Card.Body>
            </Card>
            </Col>
            <Col xs={12} md={4}>
            <Card style={{ width: '18rem' }} className="cardHighlight p-3">
                <Card.Body>
                    <Card.Title>
                        <h2>Be Part of Our Community</h2>
                    </Card.Title>
                    <Card.Text>
                    Lorem Ipsum, dizgi ve baskı endüstrisinde kullanılan mıgır metinlerdir. Lorem Ipsum, adı bilinmeyen bir matbaacının bir hurufat numune kitabı oluşturmak üzere bir yazı galerisini alarak karıştırdığı 1500'lerden beri endüstri standardı sahte metinler olarak kullanılmıştır. 
                    </Card.Text>
                </Card.Body>
            </Card>
            </Col>
        </Row>
        
    )
}